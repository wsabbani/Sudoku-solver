#include "grid.h"
#include "game.h"

#include <iostream>

const int kWindowSize{ 604 };

Game::Game()
	:m_window(sf::VideoMode(kWindowSize, kWindowSize), "Sudoku 2.0", sf::Style::Close)
{
	m_window.setFramerateLimit(60);
	m_grid.Fill_Grid();
}

void Game::Run()
{
	while (m_window.isOpen()) {
		ProcessEvents();
		Update();
		Render();
	}
}

void Game::ProcessEvents()
{
	sf::Event event{};
	while (m_window.pollEvent(event)) {
		if (event.type == sf::Event::Closed) {
			m_window.close();
		}
		if (event.type == sf::Event::MouseButtonPressed)
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				std::cout << static_cast<char>(event.text.unicode);
			}
		}
	}
}


void Game::Update()
{
}

void Game::Render()
{
	m_grid.Draw(m_window);
}
