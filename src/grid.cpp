#include "grid.h"

#include <random>

static std::random_device  Seed;
static std::mt19937        RandomEngine{ std::mt19937{Seed()} };
static std::uniform_int_distribution<int> digit(1, 9);

const int kWindowSize{ 604 };
const int kGridOutlineThickness{ 8 };
const float kGridSize{ kWindowSize - 2 * kGridOutlineThickness };

const float kCellSize{ 64.f };
const float kGridBoundaryThickness{ 8.f };
const float kSubGridBoundaryThickness{ 2.f };

Grid::Grid()
{
	/*
	Grid
	*/
	auto bg_colour{ sf::Color(0xFF9C00FF) };
	auto outline_colour{ sf::Color(0x000000FF) };
	m_grid.setSize(sf::Vector2f(kGridSize, kGridSize));
	m_grid.setFillColor(bg_colour);
	m_grid.setOutlineColor(outline_colour);
	m_grid.setOutlineThickness(kGridOutlineThickness);
	sf::Vector2f grid_offset(kGridOutlineThickness, kGridOutlineThickness);
	m_grid.setPosition(grid_offset);

	/*
	Vertical lines
	*/
	auto vline{ sf::RectangleShape{} };
	vline.setFillColor(outline_colour);
	for (auto i{ 0 }; i < 9; ++i) {
		if (i % 3 != 0) {
			vline.setSize(sf::Vector2f(kSubGridBoundaryThickness, 9 * kCellSize + 6 * kSubGridBoundaryThickness + 2 * kGridBoundaryThickness));
			vline.setPosition(grid_offset + sf::Vector2f(i * kCellSize + (i / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness), 0.f));
		}
		else {
			vline.setSize(sf::Vector2f(kGridBoundaryThickness, 9 * kCellSize + 6 * kSubGridBoundaryThickness + 2 * kGridBoundaryThickness));
			vline.setPosition(grid_offset + sf::Vector2f(i * kCellSize + (i / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness), 0.f));
		}
		m_vlines.emplace_back(vline);
	}

	/*
	Horizontal lines
	*/
	auto hline{ sf::RectangleShape{} };
	hline.setFillColor(outline_colour);
	for (auto i{ 0 }; i < 9; ++i) {
		if (i % 3 != 0) {
			hline.setSize(sf::Vector2f(9 * kCellSize + 6 * kSubGridBoundaryThickness + 2 * kGridBoundaryThickness, kSubGridBoundaryThickness));
			hline.setPosition(grid_offset + sf::Vector2f(0.f, i * kCellSize + (i / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness)));
		}
		else {
			hline.setSize(sf::Vector2f(9 * kCellSize + 6 * kSubGridBoundaryThickness + 2 * kGridBoundaryThickness, kGridBoundaryThickness));
			hline.setPosition(grid_offset + sf::Vector2f(0.f, i * kCellSize + (i / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness)));
		}
		m_hlines.emplace_back(hline);
	}

	m_font.loadFromFile("assets/fonts/JetBrainsMono-Regular.ttf");

}

void Grid::Select_Cell(int x, int y)
{
	sf::Vector2f grid_offset(kGridOutlineThickness, kGridOutlineThickness);
	int i{ -1 };
	int j{ -1 };
	if (x > grid_offset.x && x < grid_offset.x + 3 * kCellSize) {
		i = static_cast<int>((x - grid_offset.x) / kCellSize);
	}
	else if (x > grid_offset.x + 3 * kCellSize + kGridBoundaryThickness && x < grid_offset.x + 6 * kCellSize + kGridBoundaryThickness) {
		i = static_cast<int>((x - (grid_offset.x + 3 * kCellSize + kGridBoundaryThickness)) / kCellSize) + 3;
	}
	else if (x > grid_offset.x + 6 * kCellSize + 2 * kGridBoundaryThickness && x < (kWindowSize - kGridOutlineThickness)) {
		i = static_cast<int>((x - (grid_offset.x + 6 * kCellSize + 2 * kGridBoundaryThickness)) / kCellSize) + 6;
	}

	if (y > grid_offset.y && y < grid_offset.y + 3 * kCellSize) {
		j = static_cast<int>((y - grid_offset.y) / kCellSize);
	}
	else if (y > grid_offset.y + 3 * kCellSize + kGridBoundaryThickness && y < grid_offset.y + 6 * kCellSize + kGridBoundaryThickness) {
		j = static_cast<int>((y - (grid_offset.y + 3 * kCellSize + kGridBoundaryThickness)) / kCellSize) + 3;
	}
	else if (y > grid_offset.y + 6 * kCellSize + 2 * kGridBoundaryThickness && y < (kWindowSize - kGridOutlineThickness)) {
		j = static_cast<int>((y - (grid_offset.y + 6 * kCellSize + 2 * kGridBoundaryThickness)) / kCellSize) + 6;
	}

	if (i != (-1) && j != (-1)) {
		sf::Vector2f line_offset{};
		m_cell.setFillColor(sf::Color::Magenta);
		m_cell.setSize(sf::Vector2f(kCellSize, kCellSize));
		if (i % 3 != 0) {
			line_offset.x = i * kCellSize + (i / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness);
		}
		else {
			line_offset.x = i * kCellSize + (i / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness) + kGridBoundaryThickness;
		}
		if (j % 3 != 0) {
			line_offset.y = j * kCellSize + (j / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness);
		}
		else {
			line_offset.y = j * kCellSize + (j / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness) + kGridBoundaryThickness;
		}
		m_cell.setPosition(grid_offset + line_offset);
		Grid::set_text(std::to_string(digit(RandomEngine)), m_cell.getPosition());
	}
}

void Grid::Draw(sf::RenderWindow& window)
{
	window.clear(sf::Color(0x99D8EAFF));
	window.draw(m_grid);
	window.draw(m_cell);
	for (auto& i : m_vlines) {
		window.draw(i);
	}
	for (auto& i : m_hlines) {
		window.draw(i);
	}
	for (auto& i : m_cell_digit) {
		window.draw(i);
	}
	window.display();
}

void Grid::set_text(std::string text, sf::Vector2f v2f) {
	sf::Text cell_digit{};
	cell_digit.setString(text);
	cell_digit.setCharacterSize(50);
	cell_digit.setFont(m_font);
	cell_digit.setFillColor(sf::Color::Black);
	cell_digit.setStyle(sf::Text::Bold);

	cell_digit.setOrigin(cell_digit.getGlobalBounds().width / 2.f + cell_digit.getLocalBounds().left, cell_digit.getGlobalBounds().height / 2.f + cell_digit.getLocalBounds().top);
	cell_digit.setPosition(v2f.x + kCellSize / 2.f, v2f.y + kCellSize / 2.f);
	m_cell_digit.emplace_back(cell_digit);
}

void Grid::Fill_Grid() {
	std::ifstream grid_file("examples/grid.txt");
	if (!grid_file.is_open()) {
		std::cerr << "couldn't open file!\n";
	}
	else {
		std::string grid{};
		grid_file >> grid;

		sf::Vector2f grid_offset(kGridOutlineThickness, kGridOutlineThickness);
		sf::Vector2f line_offset{};
		m_cell.setFillColor(sf::Color::Transparent);
		m_cell.setSize(sf::Vector2f(kCellSize, kCellSize));

		// go through each cell of the grid
		for (int i{ 0 }; i < 9; i++) {
			for (int j{ 0 }; j < 9; j++) {
				if (i % 3 != 0) {
					line_offset.x = i * kCellSize + (i / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness);
				}
				else {
					line_offset.x = i * kCellSize + (i / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness) + kGridBoundaryThickness;
				}
				if (j % 3 != 0) {
					line_offset.y = j * kCellSize + (j / 3) * (kGridBoundaryThickness - kSubGridBoundaryThickness);
				}
				else {
					line_offset.y = j * kCellSize + (j / 3 - 1) * (kGridBoundaryThickness - kSubGridBoundaryThickness) + kGridBoundaryThickness;
				}
				m_cell.setPosition(grid_offset + line_offset);

				//char to string conversion
				std::string buff{};
				Grid::set_text(buff.append(1,grid.at(i+9*j)), m_cell.getPosition());
			}
		}

	}
}