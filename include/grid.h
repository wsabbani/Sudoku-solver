#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

struct Grid
{
	Grid();
	void Select_Cell(int x, int y);
	void Draw(sf::RenderWindow&);
	void Fill_Grid();

	void set_text(std::string, sf::Vector2f);

	sf::RectangleShape m_grid;
	sf::RectangleShape m_cell;
	std::vector<sf::RectangleShape> m_vlines;
	std::vector<sf::RectangleShape> m_hlines;

private:
	sf::Font m_font;
	std::vector<sf::Text> m_cell_digit;
};

