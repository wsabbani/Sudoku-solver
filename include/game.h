#pragma once

#include "grid.h"

class Game
{
public:
	Game();

	void Run();

private:
	void ProcessEvents();
	void Update();
	void Render();

private:
	sf::RenderWindow m_window;
	Grid m_grid{};
};